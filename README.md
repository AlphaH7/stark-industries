# Stark Industries
  An Ineractive form based on the upcoming Lens and SmartForm plugin

  <a href="http://stark-industries.s3-website.ap-south-1.amazonaws.com/" target="_blank">Demo</a>

# Install Dependencies
  ```
  npm install
  ```

# Development Server
  ```
  npm run start:flags*
  ```


# Build
  ```
  npm run build:flags*
  ```


> *Set flag as `dev` for Development Environment & `prod` for Production.


# -Powered By-
 <img src="http://ax.vacau.com/temp-root/images/reactpackage.png" height="140">
