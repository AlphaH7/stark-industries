import React, { Component } from "react";
import "./Styles/main.scss";
import AnalyticalForm from "./Components/AnalyticalForm/AnalyticalForm";

export default class App extends Component {
  constructor(props) {
    var localAmObj = localStorage.getItem('AM_object');
    super(props);
    this.state = {
      AM_object: JSON.parse(localAmObj) === null ? [] : JSON.parse(localAmObj) , // Locally stored Analytical Method Object
      showEmptyView: JSON.parse(localAmObj) === null ||  localAmObj == '[]' ,
      showListView: true,
      formData: null
    };
    if(JSON.parse(localAmObj) === null) localStorage.setItem('AM_object', '[]')
    this.toggleLFView = this.toggleLFView.bind(this);
  }

  componentDidMount() {
    console.log(
      `%c Stark %c  running on ${process.env.NODE_ENV} environment`,
      "font-weight: bold; font-size: 50px; color: #3A6073 ; text-shadow: 3px 3px 0 rgb(220,220,220)",
      "color: #fff; font-weight: bold; padding: 20px 0; font-size: 13px; "
    );
  }

  renderListedView() {
    return (<div className={`list-form-ctr ${this.state.showListView ? 'flipped' : ''}`}>
              <div className="list-ctr">
                <h2 className="head">{'ANALYTICAL METHOD DATABASE'}</h2>

                {
                  this.state.AM_object.map(
                    data => (
                      <div className="table-row" onClick={()=>{this.setState({formData: data, showListView: false})}} >
                        <div><span>{'#'}</span>{data.id}</div>
                        <div><span>{'Residue Type:'}</span>{data.rt}</div>
                      </div>
                    )
                  )
                }
                <button onClick={()=>{this.setState({showListView: false})}} ><p>{'+'}</p></button>
              </div>
              <AnalyticalForm AM_object={this.state.AM_object} id={this.state.formData === null ? `SI-AM-${(new Date().getTime()).toString(36)}` : this.state.formData.id} key={JSON.stringify(this.state)} updatedState={this.state.formData} toggleListView={(amObject)=>{this.setState({showListView: true, formData: null, AM_object: amObject})}}/>
           </div>)
  }

  toggleLFView(ev, amObject) {
    this.setState({
      showEmptyView: !this.state.showEmptyView,
      showListView: false,
      formData: null,
      AM_object: amObject ? amObject : JSON.parse(localStorage.getItem('AM_object')) === null ? [] : JSON.parse(localStorage.getItem('AM_object'))
    })
  }

  render() {
    return (
      <React.Fragment>
        <div className="app-container gradient-bg">
            <div className="branding"/>
            <div className="chem-bg"/>
            <div className="view-ctr">
              {
                this.state.showEmptyView
                    ? (<div className="empty-listing-ctr">
                            <h2>{'No Analytical Method added yet!'}</h2>
                            <button onClick={this.toggleLFView}>{"Add +"}</button>
                        </div>)
                    : this.renderListedView()
              }
            </div>
        </div>
      </React.Fragment>
    );
  }
}
