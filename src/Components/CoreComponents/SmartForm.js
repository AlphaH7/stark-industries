import React from 'react';

export default class SmartForm extends React.Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(event) {
    event.preventDefault();
    var errors = [],
      validated = [];
    Array.from(this.formRef.current.elements).forEach(data => {
      if (!data.validity.valid) {
        errors.push({
          element: data,
          error: data.validity
        });
      } else {
        validated.push({
          element: data
        });
        if(data.hideError) data.hideError();
      }
    });
    if (errors.length !== 0) {
      errors[0].element.focus();
      errors[0].element.showError(errors[0].element.validity);
      errors.forEach(data => {
        data.element.showError(data.element.validity);
      });
    } else {
      //Dont submit if errors exist
      // errors[0].element.hideError();
      this.props.onSubmit();
    }
  }

  render() {
    var props = { ...this.props };
    delete props.children;
    delete props.onSubmit;
    return (
      <form ref={this.formRef} onSubmit={this.submitForm} {...props} noValidate>
        {this.props.children}
      </form>
    );
  }
}
