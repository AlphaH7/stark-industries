import React from 'react';
import AppHelper from '../../Core-utils/AppHelper';

export default class SmartInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue:
        this.props.defaultValue === null
          ? this.props.scope.state[this.props.stateKey] &&
            this.props.scope.state[this.props.stateKey] !== null
            ? this.props.scope.state[this.props.stateKey]
            : ''
          : this.props.defaultValue,
      focused: null,
      error: null
    };
    this.inputRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.invalidMsg = this.invalidMsg.bind(this);
    this.validateValue = this.validateValue.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }
  componentDidMount() {
    this.inputRef.current['showError'] = this.validateValue;
    this.inputRef.current['hideError'] = ()=>{this.setState({error:null})};
  }
  static getDerivedStateFromProps(props, state) {
    if (
      props.defaultValue !== state.inputValue &&
      props.defaultValue !== null
    ) {
      return {
        inputValue: props.defaultValue
      };
    }
    return null;
  }
  handleChange(event) {
    const re = new RegExp(this.props.inputScheme);
    if (
      this.props.maxLength === null ||
      event.target.value.length <= this.props.maxLength
    ) {
      if (this.props.inputScheme !== null) {
        if (!re.test(event.target.value)) {
          event.preventDefault();
        } else {
          var newState = {},
            me = this.props.scope;
          newState[this.props.stateKey] = event.target.value;
          this.setState({ inputValue: event.target.value , error: null});
          me.setState(
            newState,
            this.props.onChange.bind(me, event.target.value)
          );
        }
      } else {
        var newState = {},
          me = this.props.scope;
        newState[this.props.stateKey] = event.target.value;
        this.setState({ inputValue: event.target.value , error: null});
        me.setState(newState, this.props.onChange.bind(me, event.target.value));
      }
    }
  }
  invalidMsg(validity) {
    if (validity == null) {
      return null;
    }
    var key =
      this.props.customErrorKey !== null
        ? this.props.customErrorKey
        : this.props.placeholder;
    if (validity.valueMissing) {
      return `${key} is required`;
    } else if (validity.typeMismatch) {
      return 'Please check the following input';
    } else if (validity.tooLong) {
      return `${key} is too long. Maximum allowed characters are ${this.props.maxLength}`;
    } else if (validity.tooShort) {
      return `${key} is too short. Minimum allowed characters are ${this.props.minLength}`;
    }
    return null;
  }
  validateValue(error) {
    this.setState({
      error: this.invalidMsg(error)
    });
  }
  onBlur() {
    this.setState({ focused: false });
  }
  onFocus() {
    this.setState({ focused: true });
  }
  render() {
    var me = this.props.scope,
      addtnlProps = {},
      validation = null;
    if (this.props.maxLength !== null) {
      addtnlProps['maxLength'] = this.props.maxLength;
    }
    if (this.props.minLength !== null) {
      addtnlProps['minLength'] = this.props.minLength;
    }
    if (this.props.required) {
      addtnlProps['required'] = this.props.required;
    }
    return (
      <div
        className={`input-group ${
          this.state.error !== null ? 'input-error' : ''
        } ${this.props.disabled ? 'disabled' : ''} ${
          this.state.inputValue.length === 0 ? 'empty-input' : 'has-value'
        }`}
        value={this.state.inputValue}
      >
        <input
          ref={this.inputRef}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          {...addtnlProps}
          placeholder={this.props.placeholder ? this.props.placeholder : ''}
          disabled={this.props.disabled}
          className={`${this.props.lowercase ? 'lowercase' : ''} ${
            this.props.inputCls
          } ${this.props.ctrCls}`}
          onChange={this.handleChange.bind(this.props.scope)}
          type={this.props.type}
          value={this.state.inputValue}
        />
        {this.state.error !== null ? (
          <div className="input-footer-ctr error">{this.state.error}</div>
        ) : null}
      </div>
    );
  }
}

SmartInput.defaultProps = {
  type: 'text',
  onChange: AppHelper.emptyFn,
  inputScheme: null,
  inputCls: '',
  iconCls: null,
  ctrCls: '',
  disabled: false,
  maxLength: null,
  isLoading: false,
  footer: null,
  required: false,
  customErrorKey: null,
  maxLength: null,
  defaultValue: null,
  minLength: null
};
