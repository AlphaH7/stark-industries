import React from 'react';
import AppHelper from '../../Core-utils/AppHelper';

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.checked
    };
    this.selectCheckbox = this.selectCheckbox.bind(this);
  }

  selectCheckbox() {
    this.setState(
      {
        checked: !this.state.checked
      },
      () => {
        this.props.onClick(this.state.checked);
      }
    );
  }

  static getDerivedStateFromProps(props, state) {
    if (state.checked !== state.checked) {
      return {
        checked: state.checked
      };
    }
    return null;
  }

  render() {
    return (
      <div
        onClick={this.selectCheckbox}
        className={`checkbox-ctr ${this.props.ctrCls}`}
      >
        <input
          type="checkbox"
          className={this.state.checked ? 'checked' : ''}
          id="cbx"
          value="on"
          style={{ display: 'none' }}
          disabled={this.props.disabled}
        />
        <label className="check">
          <svg width="18px" height="18px" viewBox="0 0 18 18">
            <path d="M1,9 L1,3.5 C1,2 2,1 3.5,1 L14.5,1 C16,1 17,2 17,3.5 L17,14.5 C17,16 16,17 14.5,17 L3.5,17 C2,17 1,16 1,14.5 L1,9 Z"></path>
            <polyline points="1 9 7 14 15 4"></polyline>
          </svg>
        </label>
      </div>
    );
  }
}

Checkbox.defaultProps = {
  onClick: AppHelper.emptyFn,
  ctrCls: '',
  disabled: false,
  checked: null
};
