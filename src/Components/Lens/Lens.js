import React, { Component } from "react";
import SearchIcon from "./SearchIcon";
import EmbaressedIcon from "./EmbaressedIcon";

export default class Lens extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputString: props.initialValue === null ? "" : props.initialValue,
      hoveredIndex: 0,
      focused: false,
      error: null,
      value: null
    };
    this.lensRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.returnFileredResults = this.returnFileredResults.bind(this);
    this.onKeyPressed = this.onKeyPressed.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.setSelectedItem = this.setSelectedItem.bind(this);
    this.invalidMsg = this.invalidMsg.bind(this);
    this.validateValue = this.validateValue.bind(this);
  }

  componentDidMount() {
    this.lensRef.current['showError'] = this.validateValue;
    this.lensRef.current['hideError'] = ()=>{this.setState({error:null})};
    this.lensRef.current.value = this.props.initialValue === null ? "" : this.props.initialValue;
  }

  validateValue(error) {
    this.setState({
      error: this.invalidMsg(error)
    });
  }

  invalidMsg(validity) {
    if (validity == null) {
      return null;
    }
    var key =
      this.props.customErrorKey !== null
        ? this.props.customErrorKey
        : this.props.placeholder;
    if (validity.valueMissing) {
      return `${key} is required`;
    }
    return null;
  }

  handleChange(event) {
    this.setState({ inputString: event.target.value, value: null });
  }

  returnFileredResults() {
    return this.state.inputString.length === 0
      ? this.props.entries
      : this.props.entries.filter(data => data.label.toLowerCase().includes(this.state.inputString.toLowerCase()));
  }

  renderHighlightedField(str) {
    var input = this.state.inputString;
    var strTemp = str.replace(new RegExp(input, "gi"), `+-+${input}+-+`);
    var strArr = strTemp.split("+-+");
    return strArr.map(data => (input === data ? <span>{input}</span> : data));
  }

  onKeyPressed(event) {
      const scrollParams = {
        behavior: "smooth",
        block: "end",
        inline: "nearest"
      };
      if (
        event.key === "ArrowDown" &&
        this.state.hoveredIndex <= this.returnFileredResults().length - 2
      ) {
        this.setState({ hoveredIndex: this.state.hoveredIndex + 1 }, () => {
          if (this.state.hoveredIndex >= 0)
            // Scrolling has been implemented using Refs and scrollIntoView
            // javascript nethod, NO LIBRARY OR jQuesry used.
            this[`resultNode${this.state.hoveredIndex}`].scrollIntoView(
              scrollParams
            );
        });
      } else if (event.key === "ArrowUp" && this.state.hoveredIndex >= 0) {
        this.setState(
          {
            hoveredIndex:
              this.state.hoveredIndex === 0 ? 0 : this.state.hoveredIndex - 1
          },
          () => {
            if (this.state.hoveredIndex >= 0)
              // Scrolling has been implemented using Refs and scrollIntoView
              // javascript nethod, NO LIBRARY OR jQuesry used.
              this[`resultNode${this.state.hoveredIndex}`].scrollIntoView(
                scrollParams
              );
          }
        );
      } else if (event.key === "Enter" && this.state.hoveredIndex >= 0) {
        this.setSelectedItem(
          this.returnFileredResults()[this.state.hoveredIndex]
        );
      }
  }

  setHoveredItem(index, data) {
    this.setState({ hoveredIndex: index });
  }

  setSelectedItem(data) {
    this.lensRef.current.value = data.label;
    this.setState({ error: null, value: data, inputString: data.label }, this.props.onOptionSelect.bind(null, data));
  }

  onBlur() {
    setTimeout(()=>{this.setState({ focused: false })}, 1000);
  }
  onFocus() {
    this.setState({ focused: true, value: null });
  }

  render() {
    return (
      <div className={`lens-ctr ${this.state.fetchingData ? "animate" : ""}`}>
        <input
          onChange={this.handleChange}
          ref={this.lensRef}
          onKeyDown={this.onKeyPressed}
          className="lens-input"
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          required={this.props.required}
          placeholder={this.props.placeholder ? this.props.placeholder : ''}
        />
        {this.state.error !== null ? (
          <div className="input-footer-ctr error">{this.state.error}</div>
        ) : null}
        {this.state.focused && this.state.value === null ? (
          this.returnFileredResults().length === 0 ? (
            <div className="result-ctr no-result-ctr" key="no-result">
              <div className="result-node">
                <EmbaressedIcon />
                <p>{"No Results Found"}</p>
              </div>
            </div>
          ) : (
            <div className="result-ctr" key={this.state.reults}>
              {this.returnFileredResults().map((data, i) => (
                <div
                  ref={element => (this[`resultNode${i}`] = element)}
                  className={`result-node ${
                    i == this.state.hoveredIndex ? "hovered" : ""
                  }`}
                  onClick={this.setSelectedItem.bind( null, data)}
                  onMouseEnter={this.setHoveredItem.bind(this, i, data)}
                  key={data.value + i}
                >

                <div
                  key={`result-node-${i}-${data.value}`}
                  className={`info-node ${data.value}`}
                >
                  {this.renderHighlightedField(data.label)}
                </div>

                </div>
              ))}
            </div>
          )
        ) : null}
      </div>
    );
  }
}

Lens.defaultProps = {
  onOptionSelect: null,
  initialValue: null,
  required: false,
  customErrorKey: null,
  placeholder: null
};
