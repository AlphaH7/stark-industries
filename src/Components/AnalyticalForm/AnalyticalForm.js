import React, { Component } from "react";
import Form from "../CoreComponents/SmartForm";
import Input from "../CoreComponents/SmartInput";
import Checkbox from "../CoreComponents/Checkbox";
import Dropdown from "../Lens/Lens";

export default class AnalyticalForm extends Component {
  constructor(props) {
    super(props);
    this.state = props.updatedState !== null
                          ? props.updatedState
                          : {
                                id:props.id,
                                lod:"",
                                loq:"",
                                reason:"",
                                rt: null,
                                showSwabForm: false,
                                showRinseForm: false,
                                swabMocCount: [],
                                rinseMocCount: [],
                                showTNTC: false,
                                useRCRinse: false
                              };
    this.onSubmit = this.onSubmit.bind(this);
    this.onOptionSelect = this.onOptionSelect.bind(this);
  }

  onSubmit() {
    var state = this.state;
    state['id'] = this.props.id;
    var amObject = this.props.AM_object, amCopy = [];
    var isEdit = false;
    amObject.forEach(
      data => {
          amCopy.push(state.id !== data.id ? data : state);
          if(state.id == data.id) isEdit = true;
      }
    );
    if(!isEdit) amCopy.push(state);
    localStorage.setItem('AM_object', JSON.stringify(amCopy));
    this.props.toggleListView(amCopy);
  }

  onOptionSelect(data) {
    this.setState({
      rt: data.label,
      id:"",
      lod:"",
      loq:"",
      reason:"",
      showSwabForm: false,
      showRinseForm: false,
      swabMocCount: [],
      rinseMocCount: []
    })
  }

  onMOCOptionSelect( i, type, data) {
    var newState = {};
    newState[`${type}-MOCType-${i}`] = data.label;
    this.setState(newState);
  }

  renderTypeAFormElements(){
    return this.state.rt === null ? null
                          : [
                              (<div className="adjacent-fields">
                                <Input
                                  placeholder={'LOD (in ppm)'}
                                  stateKey={'lod'}
                                  scope={this}
                                  type='number'
                                  required={true}/>
                                <Input
                                  placeholder={'LOQ (in ppm)'}
                                  stateKey={'loq'}
                                  scope={this}
                                  type='number'
                                  required={true}/>
                              </div>),
                              this.renderSwabConfigureForm(),
                              this.renderRinseConfigureForm(),
                              (<div className="adjacent-fields configure-btn-ctr">
                                {!this.state.showSwabForm &&
                                  (<button type="button" onClick={()=>{this.setState({showSwabForm: true})}}>
                                    <h4>{'+'}</h4>
                                    <p>{'Configure Swab Sampling Parameters'}</p>
                                  </button>)
                                }
                                {!this.state.showRinseForm &&
                                  (<button type="button" onClick={()=>{this.setState({showRinseForm: true})}}>
                                    <h4>{'+'}</h4>
                                    <p>{'Configure Rinse Sampling Parameters'}</p>
                                  </button>)
                                }
                              </div>),
                              (<Input
                                placeholder={'Reason'}
                                stateKey={'reason'}
                                scope={this}
                                required={true}/>)
                          ]
  }

  removeMOC(i) {
    var swabMocCount = this.state.swabMocCount;
    var index = swabMocCount.indexOf(i);
    if (index > -1) {
      swabMocCount.splice(index, 1);
    }
    this.setState({swabMocCount})
  }

  removeRinseMoc(i) {
    var rinseMocCount = this.state.rinseMocCount;
    var index = rinseMocCount.indexOf(i);
    if (index > -1) {
      rinseMocCount.splice(index, 1);
    }
    this.setState({rinseMocCount})
  }

  returnMocEntries() {
    return [
      {label: 'Stainless Steel', value: 'ss'},
      {label: 'Glass', value: 'glass'},
      {label: 'Teflon', value: 'teflon'},
      {label: 'Plastic', value: 'plastic'}
    ];
  }

  renderSwabConfigureForm() {

    return this.state.showSwabForm ? (
        [<h3>{"Swab Sampling Parameters"}</h3>,
          (<Input
            placeholder={'Method Used'}
            stateKey={'swabMethod'}
            scope={this}
            required={true}/>),
        (<div className="adjacent-fields">
          <Input
            placeholder={'Solvent Name'}
            stateKey={'swabSolvent'}
            scope={this}
            required={true}/>
          <Input
            placeholder={'Solvent Quantity (ml)'}
            stateKey={'swabSolventQuantity'}
            scope={this}
            type='number'
            required={true}/>
        </div>),
        (<Input
          placeholder={'Default Recovery (%)'}
          stateKey={'swabdr'}
          scope={this}
          type='number'
          required={true}/>),
        (
          this.state.swabMocCount.map((data, i) => (
              <div className="moc-ctr">
                <p>{'*'}</p>
                <Dropdown required initialValue={this.state[`swab-MOCType-${data}`]} entries={this.returnMocEntries()} onOptionSelect={this.onMOCOptionSelect.bind(this, data, 'swab')} placeholder="Target Residue Type"/>
                <Input
                  placeholder={'Recovery (%)'}
                  stateKey={`swab-moc-recoveryPercent-${data}`}
                  scope={this}
                  type='number'
                  required={true}/>
                <p onClick={this.removeMOC.bind(this, data)} className="close">{'x'}</p>
              </div>
            ))
        ),
        (
          <button type="button" onClick={()=>{this.setState({swabMocCount: this.state.swabMocCount.concat([this.state.swabMocCount.length === 0 ? 1 : this.state.swabMocCount[this.state.swabMocCount.length  - 1] + 1 ])})}}>
            <h4>{'+'}</h4>
            <p>{'Add MOC'}</p>
          </button>
        )]
    ) : null
  }

  renderTypeBSwabConfigureForm() {

    return this.state.showSwabForm ? (
        [<h3>{"Swab Sampling Parameters"}</h3>,
          <div className="checkbox-ctr">
            <p>{'Use Recovery for Swab?'}</p>
            <Checkbox checked={this.state.useRCSwab !== null ? this.state.useRCSwab : false} onClick={(useRCSwab)=>{this.setState({useRCSwab})}}/>
          </div>,
          <Input
            placeholder={'Default Recovery (%)'}
            stateKey={'typeBDRSwab'}
            type={'number'}
            scope={this}
            required={true}/>,
        (
          this.state.swabMocCount.map((data, i) => (
              <div className="moc-ctr">
                <p>{'*'}</p>
                <Dropdown required initialValue={this.state[`rinse-MOCType-${data}`]}  entries={this.returnMocEntries()} onOptionSelect={this.onMOCOptionSelect.bind(this, data, 'swab')} placeholder="Target Residue Type"/>
                <Input
                  placeholder={'Recovery (%)'}
                  stateKey={`swab-moc-recoveryPercent-${data}`}
                  scope={this}
                  type='number'
                  required={true}/>
                <p onClick={this.removeMOC.bind(this, data)} className="close">{'x'}</p>
              </div>
            ))
        ),
        (
          <button type="button" onClick={()=>{this.setState({swabMocCount: this.state.swabMocCount.concat([this.state.swabMocCount.length === 0 ? 1 : this.state.swabMocCount[this.state.swabMocCount.length  - 1] + 1 ])})}}>
            <h4>{'+'}</h4>
            <p>{'Add MOC'}</p>
          </button>
        )]
    ) : null
  }

  renderRinseConfigureForm() {
    return this.state.showRinseForm ? [
      <h3>{"Rinse Sampling Parameters"}</h3>,
      <Input
        placeholder={'Method Used'}
        stateKey={'rinseMethod'}
        scope={this}
        required={true}/>,
      <Input
        placeholder={'Default Recovery (%)'}
        stateKey={'rinse-dr'}
        scope={this}
        type='number'
        required={true}/>,
        (
          this.state.rinseMocCount.map((data, i) => (
              <div className="moc-ctr">
                <p>{'*'}</p>
                <Dropdown required initialValue={this.state[`rinse-MOCType-${data}`]}  entries={this.returnMocEntries()} onOptionSelect={this.onMOCOptionSelect.bind(this, data, 'rinse')} placeholder="Target Residue Type"/>
                <Input
                  placeholder={'Recovery (%)'}
                  stateKey={`rinse-recoveryPercent-${data}`}
                  scope={this}
                  type='number'
                  required={true}/>
                <p onClick={this.removeRinseMoc.bind(this, data)} className="close">{'x'}</p>
              </div>
            ))
        ),
        (
          <button type="button" onClick={()=>{this.setState({rinseMocCount: this.state.rinseMocCount.concat([this.state.rinseMocCount.length === 0 ? 1 : this.state.rinseMocCount[this.state.rinseMocCount.length  - 1] + 1 ])})}}>
            <h4>{'+'}</h4>
            <p>{'Add MOC'}</p>
          </button>
        )
    ] : null
  }

  renderTypeBRinseConfigureForm() {
    return this.state.showRinseForm ? [
      <h3>{"Rinse Sampling Parameters"}</h3>,
      <Input
        placeholder={'Solvent Volume'}
        stateKey={'rinse-sv'}
        scope={this}
        required={true}/>,
        <div className="checkbox-ctr">
          <p>{'Use Recovery for Swab?'}</p>
          <Checkbox checked={this.state.useRCRinse !== null ? this.state.useRCRinse : false} onClick={(useRCRinse)=>{this.setState({useRCRinse})}}/>
        </div>,
        <Input
          placeholder={'Default Recovery (%)'}
          stateKey={'typeBDRrise'}
          type={'number'}
          scope={this}
          required={true}/>,
        (
          this.state.rinseMocCount.map((data, i) => (
              <div className="moc-ctr">
                <p>{'*'}</p>
                <Dropdown required initialValue={this.state[`rinse-MOCType-${data}`]} entries={this.returnMocEntries()} onOptionSelect={this.onMOCOptionSelect.bind(this, data, 'rinse')} placeholder="Target Residue Type"/>
                <Input
                  placeholder={'Recovery (%)'}
                  stateKey={`rinse-recoveryPercent-${data}`}
                  scope={this}
                  type='number'
                  required={true}/>
                <p onClick={this.removeRinseMoc.bind(this, data)} className="close">{'x'}</p>
              </div>
            ))
        ),
        (
          <button type="button" onClick={()=>{this.setState({rinseMocCount: this.state.rinseMocCount.concat([this.state.rinseMocCount.length === 0 ? 1 : this.state.rinseMocCount[this.state.rinseMocCount.length  - 1] + 1 ])})}}>
            <h4>{'+'}</h4>
            <p>{'Add MOC'}</p>
          </button>
        )
    ] : null
  }

  renderTypeBFormElements(){
    return this.state.rt === null ? null
                          : [
                            <Input
                              placeholder={'Method Used'}
                              stateKey={'b-form-method'}
                              scope={this}
                              required={true}/>,
                              <div className="checkbox-ctr">
                                <p>{'Define TNTC and TFTC Limits?'}</p>
                                <Checkbox checked={this.state.showTNTC !== null ? this.state.showTNTC : false} onClick={(showTNTC)=>{this.setState({showTNTC})}}/>
                              </div>,
                              (
                                this.state.showTNTC ? (
                                  <div className="adjacent-fields">
                                    <Input
                                      placeholder={'TNTC Limit (in CFU)'}
                                      stateKey={'tntc'}
                                      scope={this}
                                      required={true}/>
                                    <Input
                                      placeholder={'TFTC Limit (in CFU)'}
                                      stateKey={'tftc'}
                                      scope={this}
                                      required={true}/>
                                  </div>
                                ) : null
                              ),
                              this.renderTypeBSwabConfigureForm(),
                              this.renderTypeBRinseConfigureForm(),
                              (<div className="adjacent-fields configure-btn-ctr">
                                {!this.state.showSwabForm &&
                                  (<button type="button" onClick={()=>{this.setState({showSwabForm: true})}}>
                                    <h4>{'+'}</h4>
                                    <p>{'Configure Swab Sampling Parameters'}</p>
                                  </button>)
                                }
                                {!this.state.showRinseForm &&
                                  (<button type="button" onClick={()=>{this.setState({showRinseForm: true})}}>
                                    <h4>{'+'}</h4>
                                    <p>{'Configure Rinse Sampling Parameters'}</p>
                                  </button>)
                                }
                              </div>),
                              (<Input
                                placeholder={'Reason'}
                                stateKey={'reason'}
                                scope={this}
                                required={true}/>)
                          ]
  }

  render() {

    var rtEntries = [
      {label: 'API', value: 'api'},
      {label: 'Cleaning Agent', value: 'Cleaning Agent'},
      {label: 'Bioburden', value: 'bioburden'},
      {label: 'Endotoxin', value: 'endotoxin'}
    ]

    return (<div className="form-ctr">
              <Form
                className="freelance"
                onSubmit={this.onSubmit}>
                <h3>{`${this.props.updatedState === null ? 'Add' : 'Edit'} Analytical Method`}</h3>
                <h3 className="close" onClick={this.props.toggleListView.bind(this, this.props.AM_object)}>{'X'}</h3>
                <div className="id-ctr">
                  <div>{'#'}</div>
                  <p>{this.props.id}</p>
                </div>
                <Dropdown required initialValue={this.state.rt} entries={rtEntries} onOptionSelect={this.onOptionSelect} placeholder="Target Residue Type"/>
                {['Bioburden', 'Endotoxin'].includes(this.state.rt)  ? this.renderTypeBFormElements() : this.renderTypeAFormElements()}

                <button className="submit" type="submit">
                  <h4>{'+'}</h4>
                  <p>{`${this.props.updatedState === null ? 'Add' : 'Save'} Analytical Method`}</p>
                </button>

              </Form>
           </div>)
  }

}

AnalyticalForm.defaultProps = {
  updatedState: null
}
